package org.devoxx4kids.forge.mods;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.common.MinecraftForge;

/**
 * Created by Smule on 10/11/15.
 */
@Mod(modid = Main.MODID, version = Main.VERSION)
public class Main {
    public static final String MODID = "MyMods";
    public static final String VERSION = "2.0";
    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        MinecraftForge.EVENT_BUS.register(new BlockBreakMessage());
    }

}

package org.devoxx4kids.forge.mods;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.event.world.BlockEvent;

/**
 * Created by Smule on 10/11/15.
 */
public class BlockBreakMessage {
    @SubscribeEvent
    public void sendMessage(BlockEvent.BreakEvent event) {
        event.getPlayer().addChatMessage(new ChatComponentText(EnumChatFormatting.GOLD + "You broke a block!"));
    }
}
